package com.robert.yummy.products.model;

public class Product {
    private String id, name, price, image, company;

    public Product() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Product(String id, String name, String price, String image, String company) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.company = company;
    }
}
