package com.robert.yummy.products.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.robert.yummy.R;
import com.robert.yummy.companyproducts.adapter.CompanyAdapter;
import com.robert.yummy.products.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Product> products;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();

    public ProductAdapter(Context context, ArrayList<Product> products) {
        db = FirebaseFirestore.getInstance();
        this.context=context;
        this.products=products;
        inflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.item_product, parent, false);
        ProductAdapter.MyViewHolder holder = new ProductAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.MyViewHolder holder, int position) {
        Product current = products.get(position);
        holder.txtName.setText(current.getName());
        holder.txtPrice.setText(current.getPrice());
        storageReference.child(current.getImage())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    Picasso.get().load(uri.toString()).into(holder.imgProduct);
                }).addOnFailureListener(onFailure -> {
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
        });
        holder.cardView.setOnLongClickListener(e -> {
            Toast.makeText(context, "Agregando " + current.getName() + " al carrito de compras", Toast.LENGTH_SHORT).show();
            return true;
        });
        holder.btnAdd.setOnClickListener(e-> {
            String value= holder.quantity_text_view.getText().toString();
            int num = Integer.parseInt(value);
            if(num>9){
                Toast.makeText(context, "No puede ordenar mas de 10 productos", Toast.LENGTH_SHORT).show();
            } else {
                num=num+1;
                holder.quantity_text_view.setText(""+num);
            }
        });
        holder.btnRest.setOnClickListener(e -> {
            String value= holder.quantity_text_view.getText().toString();
            int num = Integer.parseInt(value);
            if(num<1){
                Toast.makeText(context, "No puede tener valores negativos en el carrito de compras", Toast.LENGTH_SHORT).show();
            } else {
                num=num-1;
                holder.quantity_text_view.setText(""+num);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        TextView txtName, txtPrice, quantity_text_view;
        CardView cardView;
        Button btnAdd, btnRest;
        public MyViewHolder(View itemView){
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtPrice=itemView.findViewById(R.id.txtPrice);
            cardView=itemView.findViewById(R.id.cardItem);
            imgProduct=itemView.findViewById(R.id.imgProduct);
            btnAdd=itemView.findViewById(R.id.btnAdd);
            btnRest=itemView.findViewById(R.id.btnRest);
            quantity_text_view=itemView.findViewById(R.id.quantity_text_view);
        }
    }
}
