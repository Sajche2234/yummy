package com.robert.yummy.user.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.robert.yummy.MyPostApplication;
import com.robert.yummy.R;
import com.robert.yummy.home.activity.HomeActivity;
import com.robert.yummy.home_provider.activity.HomepActivity;
import com.robert.yummy.user.model.User;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {
    Spinner spGender, spRol;
    EditText edtName, edtUsername, edtEmail, edtPassword;
    Button btnSignUp;
    private FirebaseAuth auth;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        btnSignUp.setOnClickListener(e -> {
            String email, password;
            email = edtEmail.getText().toString();
            password = edtPassword.getText().toString();
            String result = validEmptyInput(edtName.getText().length(), edtPassword.getText().length(), edtEmail.getText().length(), edtUsername.getText().length());
            if(result.length()!=0){
                Toast.makeText(this, "Los siguientes campos: " + result + "son obligatorios:", Toast.LENGTH_LONG).show();
            } else {
                signUpWithEmailAndPassword(email, password);

            }
        });
    }

    private String validEmptyInput(int nameP, int passP, int emailP, int usernameP) {
        String msj="";
        if(nameP==0) { msj += "Nombre, "; }
        if(usernameP==0) { msj += "Usuario, "; }
        if(emailP==0) { msj += "Correo, "; }
        if(passP==0) { msj += "Contraseña, "; }
        return msj;
    }
    private void signUpWithEmailAndPassword(String email, String password){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, onComplete -> {
                    FirebaseUser currentUser;
                    if(onComplete.isSuccessful()){
                        currentUser = auth.getCurrentUser();
                        createAccount(currentUser);
                        if(spRol.getSelectedItem().toString().equals("Provider")) {
                            startActivity(new Intent(this, HomepActivity.class));
                            finish();
                        }
                        if(spRol.getSelectedItem().toString().equals("Client")) {
                            startActivity(new Intent(this, HomeActivity.class));
                            finish();
                        }
                    } else {
                        currentUser = null;
                        Toast.makeText(this, "Error al crear la cuenta de usuario", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void createAccount(FirebaseUser firebaseUser){
        if(firebaseUser != null){
            Map<String, Object> data = new HashMap<>();
            User user = new User();
            user.setName(edtName.getText().toString());
            user.setUsername(edtUsername.getText().toString());
            user.setEmail(edtEmail.getText().toString());
            user.setPassword(edtPassword.getText().toString());
            user.setGender(spGender.getSelectedItem().toString());
            user.setRol(spRol.getSelectedItem().toString());
            user.setId(firebaseUser.getUid());
            // Construir data.
            data.put("fullname", user.getName());
            data.put("username", user.getUsername());
            data.put("email", user.getEmail());
            data.put("gender", user.getGender());
            data.put("rol", user.getRol());
            data.put("id", user.getId()); //tmpId.toString()
            db.collection("users")
                    .document(user.getId())
                    .set(data)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(this, "Usuario creado exitosamente", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(onFailure -> {
                        Toast.makeText(this, "Error al almacenar los datos", Toast.LENGTH_SHORT).show();
                    });
        } else {
            Toast.makeText(this, "Error. El usuario no fue ingresado.", Toast.LENGTH_SHORT).show();
        }
    }
    private void connect(){
        spGender = findViewById(R.id.spGender);
        spRol = findViewById(R.id.spRol);
        edtName = findViewById(R.id.edtName);
        edtUsername = findViewById(R.id.edtUsername);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnSignUp = findViewById(R.id.btnSignUp);
        ArrayAdapter<String> myGender = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.generos));
        myGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(myGender);
        ArrayAdapter<String> myRol = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.roles));
        myRol.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRol.setAdapter(myRol);
        btnSignUp.setText(R.string.txtBtnRegister);
    }

    private void saveUser(String currentUser) {
        db.collection("users")
                .document(currentUser)
                .get()
                .addOnSuccessListener(onSuccess -> {
                    User user = new User();
                    Map<String, Object> data = onSuccess.getData();
                    user.setUsername(data.get("username").toString());
                    user.setEmail(data.get("email").toString());
                    user.setName(data.get("name").toString());
                    user.setId(data.get("id").toString());
                    if (data.containsKey("profile-image"))
                        user.setProfilePicture(data.get("profile-image").toString());
                    ((MyPostApplication) this.getApplication()).setUser(user);
                });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null) {
            saveUser(currentUser.getUid());
            startActivity(new Intent(this, HomepActivity.class));
            finish();
        }
    }
}
