package com.robert.yummy.user.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.robert.yummy.MyPostApplication;
import com.robert.yummy.R;
import com.robert.yummy.home.activity.HomeActivity;
import com.robert.yummy.home_provider.activity.HomepActivity;
import com.robert.yummy.user.model.User;

import io.opencensus.tags.Tag;

public class SignInActivity extends AppCompatActivity {
    EditText edtEmail, edtPassword;
    Button btnSignIn;
    TextView txtRegistro;
    private FirebaseAuth auth;
    private FirebaseFirestore db;
    private FirebaseUser fUser;
    private String valor="";
    private static final int REQUEST_CODE_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        // SOLICITAMOS PERMISOS
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[] {
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, REQUEST_CODE_PERMISSION);
        }
        btnSignIn.setOnClickListener(e -> {
            String email, password;
            email = edtEmail.getText().toString();
            password = edtPassword.getText().toString();
            signInWithEmailAndPassword(email, password);
        });
        txtRegistro.setOnClickListener(e -> {
            startActivity(new Intent(this, SignUpActivity.class));
            finish();
        });
        //
    }
    private void connect() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        txtRegistro = findViewById(R.id.txtRegistro);
        btnSignIn = findViewById(R.id.btnSignIn);
        btnSignIn.setText(R.string.txtBtnLogIn);
        txtRegistro.setText(R.string.txtTVAccountYet);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        String valor="";
        if(currentUser != null) {
            db.collection("users")
                    .document(currentUser.getUid())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists()) {
                                    if(document.getString("rol").equals("Provider")){
                                        startActivity(new Intent(getApplicationContext(), HomepActivity.class));
                                        finish();
                                    }
                                    if(document.getString("rol").equals("Client")){
                                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                        finish();
                                    }
                                    /*Log.d("TAG", "DocumentSnapshot data: " + document.getData());*/
                                } else {
                                    Log.d("ELSE", "No such document");
                                }
                            } else {
                                Log.d("TAG", "get failed with ", task.getException());
                            }

                        }
                    });
        }
    }

    private void signInWithEmailAndPassword(String email, String password){
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, onComplete -> {
                    FirebaseUser currentUser;
                    if(onComplete.isSuccessful()){
                        currentUser = auth.getCurrentUser();
                        db.collection("users")
                                .document(currentUser.getUid())
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                if(document.getString("rol").equals("Provider")){
                                                    startActivity(new Intent(getApplicationContext(), HomepActivity.class));
                                                    finish();
                                                }
                                                if(document.getString("rol").equals("Client")){
                                                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                                    finish();
                                                }
                                                /*Log.d("TAG", "DocumentSnapshot data: " + document.getData());*/
                                            } else {
                                                Log.d("ELSE", "No such document");
                                            }
                                        } else {
                                            Log.d("TAG", "get failed with ", task.getException());
                                        }

                                    }
                                });
                        /*startActivity(new Intent(this, HomepActivity.class));
                        finish();*/
                    } else {
                        currentUser=null;
                        Toast.makeText(this, "Debe Iniciar Sesión.", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
