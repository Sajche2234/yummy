package com.robert.yummy.purchasesumary.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.robert.yummy.R;
import com.robert.yummy.companyproducts.adapter.CompanyAdapter;
import com.robert.yummy.purchasesumary.model.Purchasesumary;

import java.util.ArrayList;

public class PurchasesumaryAdapter extends RecyclerView.Adapter<PurchasesumaryAdapter.MyViewHolder> {
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser fUser;
    private LayoutInflater inflater;
    private Context context;
    ArrayList<Purchasesumary> datas;

    public PurchasesumaryAdapter(Context context, ArrayList<Purchasesumary> datas){
        db = FirebaseFirestore.getInstance();
        this.context = context;
        this.datas = datas;
        inflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public PurchasesumaryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.item_purchasesumary, parent, false);
        PurchasesumaryAdapter.MyViewHolder holder = new PurchasesumaryAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PurchasesumaryAdapter.MyViewHolder holder, int position) {
        Purchasesumary current = datas.get(position);
        holder.lblIdPS.setText(current.getId());
        holder.txtProduct.setText(current.getProductid());
        holder.txtCant.setText(current.getCant());
        holder.txtPrice.setText(current.getPrice());
        holder.txtSubtotal.setText(current.getSubTotal());
        holder.txtTotal.setText(current.getTotal());
        holder.txtDate.setText(current.getDateR());
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView lblIdPS, txtProduct, txtCant, txtPrice, txtSubtotal, txtDate, txtTotal;
        public MyViewHolder(View itemView){
            super(itemView);
            lblIdPS=itemView.findViewById(R.id.lblIdPS);
            txtProduct=itemView.findViewById(R.id.txtProduct);
            txtCant=itemView.findViewById(R.id.txtCant);
            txtPrice=itemView.findViewById(R.id.txtPrice);
            txtSubtotal=itemView.findViewById(R.id.txtSubtotal);
            txtDate=itemView.findViewById(R.id.txtDate);
            txtTotal=itemView.findViewById(R.id.txtTotal);
        }
    }
}
