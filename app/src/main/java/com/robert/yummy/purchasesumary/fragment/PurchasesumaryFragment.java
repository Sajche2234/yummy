package com.robert.yummy.purchasesumary.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.robert.yummy.R;
import com.robert.yummy.purchasesumary.adapter.PurchasesumaryAdapter;
import com.robert.yummy.purchasesumary.model.Purchasesumary;
import com.robert.yummy.user.activity.SignInActivity;

import java.util.ArrayList;

public class PurchasesumaryFragment extends Fragment {
    FloatingActionButton floatingClose;
    RecyclerView recyclerPurcharsesumary;
    private ArrayList<Purchasesumary> psds; // Purcharse Sumary Datas
    private PurchasesumaryAdapter adapter;
    public PurchasesumaryFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_purchasesumary, container, false);
        floatingClose = rootView.findViewById(R.id.floatingClose);
        floatingClose.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(rootView.getContext(), SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        });
        recyclerPurcharsesumary=rootView.findViewById(R.id.recyclerPurcharsesumary);
        recyclerPurcharsesumary.setLayoutManager(new LinearLayoutManager(getActivity()));
        psds = new ArrayList<>();
        // Lenado de manera estatica.
        Purchasesumary ps1 = new Purchasesumary();
        ps1.setId("ORDEN # "+1);
        ps1.setProductid("Producto X" + '\n' + "Producto XX" + '\n' + "Producto XXX");
        ps1.setCant("3" + '\n' + "1" + '\n' + 2);
        ps1.setPrice("15" + '\n' + "10" + '\n' + 5);
        ps1.setSubTotal("45.00" + '\n' + "10" + '\n' + 5);
        ps1.setTotal("TOTAL: 65.00");
        ps1.setDateR("30/03/2020");
        psds.add(ps1);
        Purchasesumary ps2 = new Purchasesumary();
        ps2.setId("ORDEN # "+2);
        ps2.setProductid("Producto X" + '\n' + "Producto XXX");
        ps2.setCant("2" + '\n' + 1);
        ps2.setPrice("15" + '\n' + 5);
        ps2.setSubTotal("" + 30.00 + '\n' + 5.00);
        ps2.setTotal("TOTAL: 35.00");
        ps2.setDateR("01/05/2020");
        psds.add(ps2);
        adapter = new PurchasesumaryAdapter(getActivity(), psds);
        recyclerPurcharsesumary.setAdapter(adapter);
        return rootView;
    }
}
