package com.robert.yummy.purchasesumary.model;

public class Purchasesumary {
    private String id, productid, shoppingid, cant, price, subTotal, total, dateR;

    public Purchasesumary() {
    }

    public Purchasesumary(String id, String productid, String shoppingid, String cant, String price, String subTotal, String total, String dateR) {
        this.id = id;
        this.productid = productid;
        this.shoppingid = shoppingid;
        this.cant = cant;
        this.price = price;
        this.subTotal = subTotal;
        this.total = total;
        this.dateR = dateR;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getShoppingid() {
        return shoppingid;
    }

    public void setShoppingid(String shoppingid) {
        this.shoppingid = shoppingid;
    }

    public String getCant() {
        return cant;
    }

    public void setCant(String cant) {
        this.cant = cant;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDateR() {
        return dateR;
    }

    public void setDateR(String dateR) {
        this.dateR = dateR;
    }
}
