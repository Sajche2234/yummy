package com.robert.yummy.shopping.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.robert.yummy.R;
import com.robert.yummy.companyproducts.adapter.CompanyAdapter;
import com.robert.yummy.companyproducts.model.Company;

import java.util.ArrayList;

public class CompanyActivity extends AppCompatActivity {
    private Toolbar toolbar;
    RecyclerView recyclerCompanyShopping;
    private ArrayList<Company> companies;
    private CompanyAdapter adapter;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        connect();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.txtActBarShopping);
        companies = new ArrayList<>();
        // Llenar los datos de manera estática.
        /*Company c1 = new Company();
        c1.setId(""+1);
        c1.setName("Nombre 1");
        c1.setTelephone("13245687");
        c1.setAddress("Zona 1");
        companies.add(c1);
        Company c2 = new Company();
        c2.setId(""+2);
        c2.setName("Nombre 2");
        c2.setTelephone("9876542");
        c2.setAddress("Zona 2");
        companies.add(c2);*/
        // Llenado dinamicamente, por medio de la lectura de los datos en la base de datos.
        db = FirebaseFirestore.getInstance(); // Inicializando la BD
        // Ahora leer cada uno de los documentos.
        db.collection("company")
                .get()
                .addOnCompleteListener(onComplete -> {
                    if(onComplete.isSuccessful()){
                        for(QueryDocumentSnapshot document : onComplete.getResult()){
                            Company company = new Company();
                            company.setId(document.getId());
                            /*Log.d("CODIGO: ", document.getId());
                            Log.d("NOMBRE: ", document.getString("name"));*/
                            company.setName(document.getString("name"));
                            company.setTelephone(document.getString("telephone"));
                            company.setAddress(document.getString("address"));
                            if(document.getData().containsKey("image"))
                                company.setImage(document.getString("image"));
                            companies.add(company);
                        }
                        adapter = new CompanyAdapter(this, companies);
                        recyclerCompanyShopping.setAdapter(adapter);
                    } else {
                        Toast.makeText(this, "Error al leer el documento", Toast.LENGTH_SHORT).show();
                    }
                });


        // Llenando desde Base de Datos.
    }

    private void connect(){
        toolbar = findViewById(R.id.toolbar);
        recyclerCompanyShopping = findViewById(R.id.recyclerCompanyShopping);
        recyclerCompanyShopping.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
