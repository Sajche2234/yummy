package com.robert.yummy.shopping.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.robert.yummy.R;
import com.robert.yummy.products.adapter.ProductAdapter;
import com.robert.yummy.products.model.Product;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity {
    private Toolbar toolbar;
    RecyclerView recyclerCompanyShoppingProducts;
    private ArrayList<Product> products;
    private ProductAdapter adapter;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        connect();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.txtActBarShopping);
        Bundle extras = getIntent().getExtras();
        String idCompany = extras.getString("idCompany");
        //Toast.makeText(this, idCompany, Toast.LENGTH_SHORT).show();
        products = new ArrayList<>();
       /* Product p1 = new Product();
        p1.setId(""+1);
        p1.setName("Papas");
        p1.setPrice("20");
        products.add(p1);
        adapter = new  ProductAdapter(this, products);
        recyclerCompanyShoppingProducts.setAdapter(adapter);*/
        db = FirebaseFirestore.getInstance();
        db.collection("product")
                .whereEqualTo("company", idCompany)
                .get()
                .addOnCompleteListener(onComplete -> {
                    if(onComplete.isSuccessful()){
                        for(QueryDocumentSnapshot document : onComplete.getResult()) {
                            Product product = new Product();
                            product.setId(document.getId());
                            product.setName(document.getString("name"));
                            product.setPrice(document.getString("price"));
                            if(document.getData().containsKey("image"))
                                product.setImage(document.getString("image"));
                            products.add(product);
                        }
                        adapter = new ProductAdapter(this, products);
                        recyclerCompanyShoppingProducts.setAdapter(adapter);
                    } else {
                        Toast.makeText(this, "Error al leer el documento", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void connect(){
        toolbar = findViewById(R.id.toolbar);
        recyclerCompanyShoppingProducts = findViewById(R.id.recyclerCompanyShoppingProducts);
        recyclerCompanyShoppingProducts.setLayoutManager(new LinearLayoutManager(this));
    }
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
