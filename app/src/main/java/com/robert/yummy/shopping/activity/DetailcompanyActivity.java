package com.robert.yummy.shopping.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.robert.yummy.R;
import com.robert.yummy.companyproducts.model.Company;
import com.robert.yummy.home.activity.HomeActivity;
import com.robert.yummy.home_provider.activity.HomepActivity;

import java.util.ArrayList;

public class DetailcompanyActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private FirebaseFirestore db;
    private TextView txtCName, txtCAddress, txtCTelephone;
    private Company company = new Company();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailcompany);
        connect();
        db = FirebaseFirestore.getInstance();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Vamonos de Compras");
        Bundle extras = getIntent().getExtras();
        String idCompany = extras.getString("idCompany");
        //Toast.makeText(this, idCompany, Toast.LENGTH_SHORT).show();
        db.collection("company")
                .document(idCompany)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                txtCName.setText(document.getString("name"));
                                txtCAddress.setText(document.getString("address"));
                                txtCTelephone.setText(document.getString("telephone"));
                                //Log.d("Campo: ", document.getString("telephone"));
                                Log.d("TAG", "DocumentSnapshot data: " + document.getData());
                            } else {
                                Log.d("ELSE", "No such document");
                            }
                        } else {
                            Log.d("TAG", "get failed with ", task.getException());
                        }

                    }
                });
    }
    public  void connect(){
        toolbar = findViewById(R.id.toolbar);
        txtCName = findViewById(R.id.txtCName);
        txtCAddress = findViewById(R.id.txtCAddress);
        txtCTelephone = findViewById(R.id.txtCTelephone);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
