package com.robert.yummy.home.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.robert.yummy.R;
import com.robert.yummy.companyproducts.fragment.CompanyproductsFragment;
import com.robert.yummy.navigation.fragment.FragmentDrawer;
import com.robert.yummy.profile.fragment.ProfileFragment;
import com.robert.yummy.purchasesumary.fragment.PurchasesumaryFragment;
import com.robert.yummy.user.activity.SignInActivity;

public class HomeActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    //Button btnOut;
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser fUser;
    private Toolbar toolbar;
    private FragmentDrawer drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        /*btnOut.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        });*/

        /*drawer=(FragmentDrawer)getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawer.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawer.setDrawerListener(this);*/
        //
        BottomNavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        toolbar.setTitle(R.string.title_store);
        displayView(new CompanyproductsFragment());
        getSupportActionBar().setTitle(R.string.title_store);
        //displayView(0);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;
            switch (menuItem.getItemId()){
                case R.id.navigation_store:
                    toolbar.setTitle(R.string.title_store);
                    fragment = new CompanyproductsFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_plus:
                    toolbar.setTitle(R.string.title_sumary);
                    fragment = new PurchasesumaryFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_profile:
                    toolbar.setTitle(R.string.title_profile);
                    fragment = new ProfileFragment();
                    displayView(fragment);
                    return true;
            }
            return false;
        }
    };

    private void displayView(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }


    private void connect(){
        /*btnOut = findViewById(R.id.btnOut);
        btnOut.setText(R.string.txtBtnLogout);*/
        toolbar=findViewById(R.id.toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fUser = auth.getCurrentUser();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 0:
                fragment = new CompanyproductsFragment();
                title = getString(R.string.nav_item_company_products);
                break;
            case 1:
                fragment = new PurchasesumaryFragment();
                title = getString(R.string.nav_item_company_products);
                break;
            case 2:
                fragment = new ProfileFragment();
                title = getString(R.string.nav_item_profile);
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
    }
}
