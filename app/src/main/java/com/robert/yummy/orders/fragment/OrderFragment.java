package com.robert.yummy.orders.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.robert.yummy.R;
import com.robert.yummy.user.activity.SignInActivity;

public class OrderFragment extends Fragment {
    FloatingActionButton floatingClose;
    public OrderFragment(){}
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_orders, container, false);
        floatingClose = rootView.findViewById(R.id.floatingClose);
        floatingClose.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(rootView.getContext(), SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        });
        return rootView;
    }
}
