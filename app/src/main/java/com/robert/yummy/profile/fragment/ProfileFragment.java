package com.robert.yummy.profile.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.robert.yummy.MyPostApplication;
import com.robert.yummy.R;
import com.robert.yummy.user.activity.SignInActivity;
import com.robert.yummy.user.model.User;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProfileFragment extends Fragment {
    //FloatingActionButton floatingClose;
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private TextView edtName, edtUsername;
    private ImageView imgProfile;
    private Button btnPicker, btnUpdate;
    private static final int REQUEST_CODE_IMAGE_PICKER = 5050;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();

    public ProfileFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        edtName=rootView.findViewById(R.id.edtName);
        edtUsername=rootView.findViewById(R.id.edtUsername);
        btnPicker = rootView.findViewById(R.id.btnPick);
        btnUpdate = rootView.findViewById(R.id.btnUpdate);
        imgProfile = rootView.findViewById(R.id.imgProfile);
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        btnPicker.setOnClickListener(e -> {
            Options options = Options.init()
                    .setRequestCode(REQUEST_CODE_IMAGE_PICKER)
                    .setCount(1)
                    .setImageQuality(ImageQuality.HIGH)
                    .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)
                    .setPath("/yummy/images/image");

            Pix.start(this, options);
        });

        btnUpdate.setOnClickListener(e -> {
            Map<String, Object> data = new HashMap<>();
            data.put("fullname", edtName.getText().toString());
            data.put("username", edtUsername.getText().toString());
            data.put("id", user.getUid());
            db.collection("users").document(user.getUid()).update(data);
            Toast.makeText(rootView.getContext(), "Datos actualizados exitosamente.", Toast.LENGTH_SHORT).show();
        });
        /*floatingClose = rootView.findViewById(R.id.floatingClose);
        floatingClose.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(rootView.getContext(), SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        });*/
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        user = auth.getCurrentUser();
        if(currentUser != null) {
            Log.d("USUARIO:", currentUser.getUid());
            db.collection("users")
                    .document(currentUser.getUid())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if (task.isSuccessful()) {
                                DocumentSnapshot document = task.getResult();
                                if (document.exists()) {
                                    edtName.setText(document.getString("fullname"));
                                    edtUsername.setText(document.getString("username"));
                                    //txtCTelephone.setText(document.getString("telephone"));
                                    //Log.d("Campo: ", document.getString("telephone"));
                                    Log.d("TAG", "DocumentSnapshot data: " + document.getData());
                                } else {
                                    Log.d("ELSE", "No such document");
                                }
                            } else {
                                Log.d("TAG", "get failed with ", task.getException());
                            }

                        }
                    });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_IMAGE_PICKER  && resultCode == Activity.RESULT_OK) {

            // RECIBIMOS LISTADO DE IMAGENES
            ArrayList<String> pictures = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            File imgFile = new File(pictures.get(0));

            // CONVERTIMOS A URI
            Uri uri = Uri.fromFile(imgFile);
            // REFERENCIA HACIA EL STORATE
            StorageReference profileReference = storageReference.child("image/").child(uri.getLastPathSegment());
            UploadTask uploadTask = profileReference.putFile(uri);

            // SUBIDA DE ARHIVO
            uploadTask.addOnFailureListener(onFailure -> {
                Toast.makeText(getActivity(), "1 - Error al subir imagen", Toast.LENGTH_SHORT).show();
            }).addOnSuccessListener(onSuccess -> {

                try {
                    storageReference.child(((MyPostApplication) getContext()).getUser().getProfilePicture())
                            .delete(); // Error en getContext()
                } catch (Exception e) {
                    Toast.makeText(getContext(), "No hay imagen para eliminar", Toast.LENGTH_SHORT).show();
                }


                // ACTUALIZAR BASE DE DATOS
                Map<String, Object> dataFirebase = new HashMap<>();
                dataFirebase.put("image", "image/" + uri.getLastPathSegment());

                db.collection("users")
                        .document(user.getUid())
                        .update(dataFirebase)
                        .addOnSuccessListener(avoid -> {
                            Toast.makeText(getContext(), "Subida exitosa", Toast.LENGTH_SHORT).show();
                            ((MyPostApplication) getContext()).getUser().setProfilePicture("image/"+uri.getLastPathSegment());
                            loadImage();
                        })
                        .addOnFailureListener(onFailed -> {
                            Toast.makeText(getContext(), "2 - Almacenamiento erroneo", Toast.LENGTH_SHORT).show();
                        });

            });

        }
    }

    private void loadImage() {
        User user = ((MyPostApplication) getContext()).getUser();
        storageReference.child(user.getProfilePicture())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    //new DownloadImageThread().execute(uri.toString()); // Ejecucion del hilo
                    Picasso.get().load(uri.toString()).into(imgProfile);
                }).addOnFailureListener(onFailure -> {
            Toast.makeText(getContext(), "Error al cargar imagen", Toast.LENGTH_SHORT).show();
        });
    }
}
