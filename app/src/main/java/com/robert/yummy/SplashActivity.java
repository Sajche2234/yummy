package com.robert.yummy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.robert.yummy.user.activity.SignInActivity;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    ImageView imgSplashRotate;
    TextView txtSplash;
    private static final long SCREEN_DELAY=3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        connect();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                startActivity(intent);
                finish();
            }
        }, SCREEN_DELAY);
    }

    private void connect(){
        imgSplashRotate = findViewById(R.id.imgSplashRotate);
        rotarImagen(imgSplashRotate);
        txtSplash = findViewById(R.id.txtSplash);
        txtSplash.setText(R.string.lblLoading);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    private void rotarImagen(View view){
        RotateAnimation animationR = new RotateAnimation(0, 360, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        animationR.setDuration(2000);
        animationR.setRepeatCount(Animation.INFINITE);
        animationR.setRepeatMode(Animation.RESTART);
        view.startAnimation(animationR);
    }
}
