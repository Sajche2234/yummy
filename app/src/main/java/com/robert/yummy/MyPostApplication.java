package com.robert.yummy;

import android.app.Application;

import com.robert.yummy.user.model.User;

public class MyPostApplication extends Application {
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
