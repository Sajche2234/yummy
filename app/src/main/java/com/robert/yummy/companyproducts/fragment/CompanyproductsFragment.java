package com.robert.yummy.companyproducts.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.robert.yummy.R;
import com.robert.yummy.companyproducts.adapter.CompanyAdapter;
import com.robert.yummy.companyproducts.model.Company;
import com.robert.yummy.shopping.activity.CompanyActivity;
import com.robert.yummy.user.activity.SignInActivity;

import java.util.ArrayList;

public class CompanyproductsFragment extends Fragment {
    FloatingActionButton floatingClose, floatingShopping;
    RecyclerView recyclerCompany;
    private ArrayList<Company> companies;
    private CompanyAdapter adapter;
    private FirebaseFirestore db;
    public CompanyproductsFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_companyproducts, container, false);
        floatingClose = rootView.findViewById(R.id.floatingClose);
        floatingShopping = rootView.findViewById(R.id.floatingShopping);
        floatingClose.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(rootView.getContext(), SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        });
        floatingShopping.setOnClickListener(r -> {
            startActivity(new Intent(getActivity(), CompanyActivity.class));
            //getActivity().finish();
        });
        recyclerCompany=rootView.findViewById(R.id.recyclerCompany);
        recyclerCompany.setLayoutManager(new LinearLayoutManager(getActivity()));
        companies = new ArrayList<>();
        // Llenar los datos de manera estática.
        db = FirebaseFirestore.getInstance();
        db.collection("company")
                .get()
                .addOnCompleteListener(onComplete -> {
                    if(onComplete.isSuccessful()){
                        for(QueryDocumentSnapshot document : onComplete.getResult()){
                            Company company = new Company();
                            company.setId(document.getId());
                            company.setName(document.getString("name"));
                            company.setTelephone(document.getString("telephone"));
                            company.setAddress(document.getString("address"));
                            if(document.getData().containsKey("image"))
                                company.setImage(document.getString("image"));
                            companies.add(company);
                        }
                        adapter = new CompanyAdapter(getActivity(), companies);
                        recyclerCompany.setAdapter(adapter);
                    } else {
                        Toast.makeText(getActivity(), "Error al leer el documento", Toast.LENGTH_SHORT).show();
                    }
                });
        return  rootView;
    }
    public void refresh(FirebaseFirestore db){
        db.collection("company")
                .get()
                .addOnCompleteListener(onComplete -> {
                    if(onComplete.isSuccessful()){
                        for(QueryDocumentSnapshot document : onComplete.getResult()){
                            Company company = new Company();
                            company.setId(document.getId());
                            company.setName(document.getString("name"));
                            company.setTelephone(document.getString("telephone"));
                            company.setAddress(document.getString("address"));
                            companies.add(company);
                        }
                        adapter = new CompanyAdapter(getActivity(), companies);
                        recyclerCompany.setAdapter(adapter);
                    } else {
                        Toast.makeText(getActivity(), "Error al leer el documento", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
