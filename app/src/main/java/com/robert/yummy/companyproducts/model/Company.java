package com.robert.yummy.companyproducts.model;

public class Company {
    private String id, name, address, telephone, nit, image;

    public Company() {
    }

    public Company(String id, String name, String address, String telephone, String nit, String image) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.telephone = telephone;
        this.nit = nit;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
