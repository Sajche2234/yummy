package com.robert.yummy.companyproducts.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.robert.yummy.R;
import com.robert.yummy.companyproducts.model.Company;
import com.robert.yummy.shopping.activity.CompanyActivity;
import com.robert.yummy.shopping.activity.DetailcompanyActivity;
import com.robert.yummy.shopping.activity.ProductActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.MyViewHolder> {
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser fUser;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Company> companies;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();

    public CompanyAdapter(Context context, ArrayList<Company> companies){
        db = FirebaseFirestore.getInstance();
        this.context = context;
        this.companies=companies;
        inflater=LayoutInflater.from(context);
    }

    /***
     * Inflo la vista del item en el recyclervie
     * @param parent
     * @param viewType
     * @return
     */

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.item_company, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    /***
     * Para interactuar con nuestro item actual o seleccionado
     * @param holder
     * @param position
     */

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Company current = companies.get(position);

        holder.txtName.setText(current.getName());
        holder.txtTelephone.setText(current.getTelephone());
        holder.txtAddress.setText(current.getAddress());
        // Creo que coloco los valores
        storageReference.child(current.getImage())
                .getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    Picasso.get().load(uri.toString()).into(holder.imgCompany);
                }).addOnFailureListener(onFailure -> {
                    //Toast.makeText(current, "Error al cargar la imagen", Toast.LENGTH_SHORT).show();
        });
        holder.cardView.setOnClickListener(e->{
            db.collection("company")
                    .document(current.getId())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            DocumentSnapshot document = task.getResult();
                            Intent i = new Intent(context, ProductActivity.class);
                            i.putExtra("idCompany", document.getId());
                            context.startActivity(i);
                            //Toast.makeText(context, document.getId(), Toast.LENGTH_SHORT).show();
                        }
                    });
        });

        holder.cardView.setOnLongClickListener(e -> {
            db.collection("company")
                    .document(current.getId())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            DocumentSnapshot document = task.getResult();
                            //Toast.makeText(context, document.getId(), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(context, DetailcompanyActivity.class);
                            i.putExtra("idCompany", document.getId());
                            context.startActivity(i);
                        }
                    });
            return true;
        });
    }

    /***
     * Obtiene el tamaño del arraylist cargado en el recyclerview
     * @return
     */
    // Regresar la cantidad de items que tiene el recyclerview.
    @Override
    public int getItemCount() {
        return companies.size();
    }

    /***
     * 1) Conectarse con el item
     */
    // Nos servira para conectarnos con el item.
    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgCompany;
        TextView txtName, txtTelephone, txtAddress;
        CardView cardView;
        public MyViewHolder(View itemView){
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtTelephone=itemView.findViewById(R.id.txtTelephone);
            txtAddress=itemView.findViewById(R.id.txtAddress);
            cardView=itemView.findViewById(R.id.cardItem);
            // Pendiente cargar la imagen
            imgCompany=itemView.findViewById(R.id.imgCompany);
        }
    }

}
