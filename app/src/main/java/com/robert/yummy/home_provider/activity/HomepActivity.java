package com.robert.yummy.home_provider.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.robert.yummy.R;
import com.robert.yummy.company.fragment.CompanyFragment;
import com.robert.yummy.companyproducts.fragment.CompanyproductsFragment;
import com.robert.yummy.navigation.fragment.FragmentDrawer;
import com.robert.yummy.orders.fragment.OrderFragment;
import com.robert.yummy.products.fragment.ProductFragment;
import com.robert.yummy.profile.fragment.ProfileFragment;
import com.robert.yummy.purchasesumary.fragment.PurchasesumaryFragment;
import com.robert.yummy.user.activity.SignInActivity;
import com.robert.yummy.user.model.User;

import java.util.ArrayList;

public class HomepActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    //Button btnOut;
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser fUser;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homep);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*btnOut.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        });*/
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        BottomNavigationView navigationView = findViewById(R.id.navigation2);
        navigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        displayView(new ProductFragment());
        getSupportActionBar().setTitle(R.string.title_products);
        //displayView(0);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment fragment;
            switch (menuItem.getItemId()){
                case R.id.navigation_products:
                    toolbar.setTitle(R.string.title_products);
                    fragment = new ProductFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_orders:
                    toolbar.setTitle(R.string.title_orders);
                    fragment = new OrderFragment();
                    displayView(fragment);
                    return true;
                case R.id.navigation_company:
                    toolbar.setTitle(R.string.title_company);
                    fragment = new CompanyFragment();
                    displayView(fragment);
                    return true;
            }
            return false;
        }
    };

    private void displayView(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }

    private void connect(){
        /*btnOut = findViewById(R.id.btnOut);
        btnOut.setText(R.string.txtBtnLogout);*/
        toolbar=findViewById(R.id.toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        fUser = auth.getCurrentUser();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }
    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 0:
                fragment = new CompanyproductsFragment();
                title = getString(R.string.nav_item_company_products);
                Log.d("QUE", "Fragmento 1");
                break;
            case 1:
                fragment = new PurchasesumaryFragment();
                title = getString(R.string.nav_item_company_products);
                Log.d("QUE", "Fragmento 2");
                break;
            case 2:
                fragment = new ProfileFragment();
                title = getString(R.string.nav_item_profile);
                Log.d("QUE", "Fragmento 3");
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
    }
}
